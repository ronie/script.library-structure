import os
import sys
import xbmc
import xbmcaddon

ADDON = xbmcaddon.Addon()
ADDONID = ADDON.getAddonInfo('id')
ADDONNAME = ADDON.getAddonInfo('name')
ADDONVERSION = ADDON.getAddonInfo('version')
LANGUAGE = ADDON.getLocalizedString
CWD = ADDON.getAddonInfo('path')

from lib.utils import *

if ( __name__ == '__main__' ):
    log('script version %s started' % ADDONVERSION)
    from lib import gui
    ui = gui.GUI('script-librarystructure.xml', CWD, 'default')
    ui.doModal()
    del ui
    log('script version %s ended' % ADDONVERSION)
